﻿Public Class Dispositivo
    Private miOID As Integer
    Private miOIDOrigen As Integer
    Private miOIDDestino As Integer

    Public Property OID As Integer
        Get
            Return miOID
        End Get
        Set(value As Integer)
            miOID = value
        End Set
    End Property

    Public Property OIDOrigen As Integer
        Get
            Return miOIDOrigen
        End Get
        Set(value As Integer)
            miOIDOrigen = value
        End Set
    End Property

    Public Property OIDDestino As Integer
        Get
            Return miOIDDestino
        End Get
        Set(value As Integer)
            miOIDDestino = value
        End Set
    End Property
End Class
