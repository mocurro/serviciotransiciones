﻿
Public Class Ordenes
    Public Property OID As Integer
    Public Property Codigo As String
    Public Property Notas As String
    Public Property Consecutivo As Nullable(Of Integer)
    Public Property Metadato As Nullable(Of Integer)
    Public Property OIDOrdenesTipos As Nullable(Of Integer)
    Public Property Estatus As Nullable(Of Integer)
    Public Property Fecha As Nullable(Of Date)
    Public Property Importación As Nullable(Of Date)
    Public Property OptimisticLockField As Nullable(Of Integer)
    Public Property GCRecord As Nullable(Of Integer)
    Public Property AutoGenerado As Nullable(Of Boolean)

    Public Overridable Property OrdenesTipos As OrdenesTipos

End Class
