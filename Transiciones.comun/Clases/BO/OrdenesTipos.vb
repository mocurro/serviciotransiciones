﻿Public Class OrdenesTipos
    Public Property OID As Integer
    Public Property Nombre As String
    Public Property Codigo As String
    Public Property Estatus As Nullable(Of Integer)
    Public Property OptimisticLockField As Nullable(Of Integer)
    Public Property GCRecord As Nullable(Of Integer)

End Class
