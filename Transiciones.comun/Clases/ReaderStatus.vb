﻿Public Class ReaderStatus
    Private Mistatus As String
    Private Mitime As String
    Private MiserialNumber As String
    Private MimqttBrokerConnectionStatus As String

    Public Property status As String
        Get
            Return Mistatus
        End Get
        Set(value As String)
            Mistatus = value
        End Set
    End Property

    Public Property time As String
        Get
            Return Mitime
        End Get
        Set(value As String)
            Mitime = value
        End Set
    End Property

    Public Property serialNumber As String
        Get
            Return MiserialNumber
        End Get
        Set(value As String)
            MiserialNumber = value
        End Set
    End Property

    Public Property mqttBrokerConnectionStatus As String
        Get
            Return MimqttBrokerConnectionStatus
        End Get
        Set(value As String)
            MimqttBrokerConnectionStatus = value
        End Set
    End Property
End Class
