﻿Imports System.Data.SqlClient

Public Class ConexionBD

    Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

    Private Orden As New Ordenes

    'Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

    ''' <summary>
    ''' Obtiene el reader 
    ''' </summary>
    ''' <param name="Nombre">nombre del reader</param>
    ''' <returns>Reader</returns>
    Public Function getDispositivo(ByVal Nombre As String) As Dispositivo
        Dim elemento As Dispositivo = Nothing
        Using conn As New SqlConnection(My.Settings.ConexionSQL)
            conn.Open()

            Using command As New SqlCommand(My.Settings.getDispositivo, conn)
                command.CommandType = CommandType.StoredProcedure
                command.Parameters.Add("@Nombre", SqlDbType.Text).Value = Nombre
                Dim data As SqlDataReader
                data = command.ExecuteReader()

                If (data.HasRows) Then
                    If data.Read() Then
                        elemento = New Dispositivo
                        elemento.OID = data(0).ToString
                        If (String.IsNullOrEmpty(data(1).ToString)) Then
                        Else
                            elemento.OIDOrigen = data(1).ToString
                        End If

                        If (String.IsNullOrEmpty(data(2).ToString)) Then
                        Else
                            elemento.OIDDestino = data(2).ToString
                        End If


                    End If

                End If
            End Using
        End Using
        Return elemento
    End Function

    ''' <summary>
    ''' Obtiene el id del elemento y el id de la orden, si no existe la inserta
    ''' </summary>
    ''' <param name="TagInventory">TagInventory</param>
    ''' <param name="Dispositivo">Dispositivo</param>
    ''' <returns>EL id del elemento y la orden</returns>
    Public Sub getElementoOrden(ByVal inventory As TagInventory, ByVal Dispositivo As Dispositivo)
        Try
            Dim OIDElemento As Integer
            Dim OIDOrden As Integer
            Dim OIDOrdenDetalle As Integer
            Dim EPCHex As String = inventory.epcHex
            Dim fecha As String = inventory.lastSeenTime

            Using conn As New SqlConnection(My.Settings.ConexionSQL)
                conn.Open()

                Using command As New SqlCommand(My.Settings.getElementoOrden, conn)
                    command.CommandType = CommandType.StoredProcedure
                    command.Parameters.Add("@EpcHex", SqlDbType.Text).Value = EPCHex
                    Dim data As SqlDataReader
                    data = command.ExecuteReader()
                    If (data.HasRows) Then
                        While data.Read
                            If data.HasRows = True Then
                                If data(0) Is Nothing Or String.IsNullOrEmpty(data(0)) Then
                                    command.CommandText = My.Settings.insertElemento
                                    command.CommandType = CommandType.StoredProcedure
                                    command.Parameters.Add("@EpcHex", SqlDbType.Text).Value = EPCHex
                                    data = command.ExecuteReader()
                                    While data.Read
                                        OIDElemento = data(0).ToString
                                    End While
                                Else
                                    OIDElemento = data(0).ToString
                                    If (String.IsNullOrEmpty(data(1).ToString)) Then
                                        'no hay orden
                                    Else
                                        OIDOrden = data(1).ToString
                                    End If

                                    OIDOrdenDetalle = 0

                                    If (String.IsNullOrEmpty(data(2).ToString)) Then
                                        'no hay orden detalle
                                    Else
                                        OIDOrdenDetalle = data(2).ToString
                                    End If

                                    If OIDOrden > 0 Then
                                        If Orden.OID <> OIDOrden Then
                                            data.Close()
                                            command.CommandText = My.Settings.getOrdenID
                                            command.Parameters.Clear()
                                            command.Parameters.Add("@OID", SqlDbType.Int).Value = OIDOrden
                                            data = command.ExecuteReader()

                                            While data.Read
                                                'OID	Codigo	Notas	Consecutivo	Metadato	OIDOrdenesTipos	Estatus	Fecha	Importación	OptimisticLockField	GCRecord	AutoGenerado
                                                '5      104		            1	        0	      1	            1	    2020-09-08 09:17:55.187	2020-09-08 09:17:55.187	NULL	NULL	NULL
                                                Orden.OID = data(0).ToString
                                                Orden.Codigo = data(1).ToString
                                                Orden.Consecutivo = data(3).ToString
                                                Orden.OIDOrdenesTipos = data(5).ToString
                                                Orden.Estatus = data(6).ToString
                                            End While
                                        End If
                                    End If

                                    If OIDOrdenDetalle > 0 Then
                                        data.Close()
                                        command.CommandText = My.Settings.updateOrdenDetalle
                                        command.Parameters.Clear()
                                        command.Parameters.Add("@OID", SqlDbType.Int).Value = OIDOrdenDetalle
                                        command.Parameters.Add("@estatus", SqlDbType.Int).Value = Orden.OIDOrdenesTipos
                                        data = command.ExecuteReader()

                                        data.Close()
                                        command.CommandText = My.Settings.UpdateOrden
                                        command.Parameters.Clear()
                                        command.Parameters.Add("@OIDOrden", SqlDbType.Int).Value = Orden.OID
                                        command.Parameters.Add("@EstatusDetalle", SqlDbType.Int).Value = Orden.OIDOrdenesTipos
                                        command.Parameters.Add("@EstatusOrden", SqlDbType.Int).Value = 3
                                        data = command.ExecuteReader()
                                    End If
                                End If
                            End If
                        End While
                    Else
                        data.Close()

                        command.CommandText = My.Settings.insertElemento
                        data = command.ExecuteReader()
                        While data.Read
                            OIDElemento = data(0).ToString
                        End While
                    End If


                    'Insertar la transición con los datos recolectados
                    data.Close()
                    command.CommandText = My.Settings.InsertTransiciones
                    command.Parameters.Clear()
                    command.Parameters.Add("@OIDOrigen", SqlDbType.Int).Value = Dispositivo.OIDOrigen ' IIf(Dispositivo.OIDOrigen = 0, Nothing, Dispositivo.OIDOrigen)
                    command.Parameters.Add("@OIDestino", SqlDbType.Int).Value = Dispositivo.OIDDestino 'IIf(Dispositivo.OIDDestino = 0, Nothing, Dispositivo.OIDDestino)
                    command.Parameters.Add("@OIDispositivo", SqlDbType.Int).Value = Dispositivo.OID
                    command.Parameters.Add("@OIDElemento", SqlDbType.Int).Value = OIDElemento
                    If (OIDOrden > 0) Then
                        command.Parameters.Add("@Tipo", SqlDbType.Int).Value = IIf(Orden.OIDOrdenesTipos Is Nothing, 2, Orden.OIDOrdenesTipos)
                        command.Parameters.Add("@OIDOrden", SqlDbType.Int).Value = Orden.OID
                    Else
                        command.Parameters.Add("@Tipo", SqlDbType.Int).Value = 2
                        command.Parameters.Add("@OIDOrden", SqlDbType.Int).Value = OIDOrden
                    End If

                    command.Parameters.Add("@Cantidad", SqlDbType.Int).Value = 1
                    command.Parameters.Add("@FechaRegistro", SqlDbType.DateTime).Value = fecha
                    command.Parameters.Add("@OIDOrdenDetalle", SqlDbType.Int).Value = OIDOrdenDetalle

                    command.Parameters.Add("@AntenaName", SqlDbType.Text).Value = inventory.antennaName
                    command.Parameters.Add("@AntenaPort", SqlDbType.Text).Value = inventory.antennaPort
                    command.Parameters.Add("@frequency", SqlDbType.Float).Value = inventory.frequency
                    command.Parameters.Add("@RSSI", SqlDbType.Float).Value = inventory.peakRssiCdbm
                    command.Parameters.Add("@PhaseAngle", SqlDbType.Float).Value = inventory.phaseAngle

                    data = command.ExecuteReader()
                    While data.Read
                        OIDElemento = data(0).ToString
                    End While


                End Using

                conn.Close()
            End Using
        Catch ex As Exception
            Dim X = ex.Message()

        End Try
    End Sub


    Public Sub InsertReaderEstatus(ByVal dispositivo As Dispositivo, ByVal fechareader As String, ByVal estatus As Integer, ByVal evento As String)
        Try
            Dim OIDElemento As Integer
            Using conn As New SqlConnection(My.Settings.ConexionSQL)
                conn.Open()

                Using command As New SqlCommand(My.Settings.getElementoOrden, conn)
                    command.CommandType = CommandType.StoredProcedure
                    Dim data As SqlDataReader

                    command.CommandText = My.Settings.InsertReaders
                    command.Parameters.Add("@dispositivo", SqlDbType.Int).Value = dispositivo.OID
                    command.Parameters.Add("@FechaReader", SqlDbType.DateTime).Value = fechareader
                    command.Parameters.Add("@Estatus", SqlDbType.Int).Value = estatus
                    command.Parameters.Add("@Evento", SqlDbType.Text).Value = evento

                    data = command.ExecuteReader()
                    While data.Read
                        OIDElemento = data(0).ToString
                    End While

                End Using

                conn.Close()
            End Using
        Catch ex As Exception

        End Try
    End Sub



    ''Funciones de la versión 2

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="Sentido"></param>
    ''' <param name="EPC"></param>
    ''' <param name="AntennaPort"></param>
    ''' <param name="LastSeenTime"></param>
    ''' <param name="PhaseAngleInRadians"></param>
    ''' <param name="PeakRssiInDbm"></param>
    ''' <param name="SeenCount"></param>
    Public Sub InsertMovimiento(ByVal Sentido As Integer, ByVal EPC As String, ByVal AntennaPort As Integer, ByVal LastSeenTime As DateTime, ByVal PhaseAngleInRadians As Double, ByVal PeakRssiInDbm As Double, ByVal SeenCount As Double, ByVal TID As String)
        Try

            Dim OIDtag, oidMovimiento As Integer
            Using conn As New SqlConnection(My.Settings.ConexionSQLV2)

                conn.Open()
                Using command As New SqlCommand(My.Settings.getTag, conn)
                    Dim data As SqlDataReader


                    command.CommandType = CommandType.StoredProcedure
                    command.Parameters.Add("@EpcHex", SqlDbType.Text).Value = EPC
                    data = command.ExecuteReader()
                    If (data.HasRows) Then
                        While data.Read
                            If data.HasRows = True Then
                                OIDtag = data(0).ToString
                            End If
                        End While
                    Else

                        data.Close()
                        command.Parameters.Clear()
                        command.CommandText = My.Settings.InsertTag
                        command.CommandType = CommandType.StoredProcedure
                        command.Parameters.Add("@EpcHex", SqlDbType.Text).Value = EPC
                        command.Parameters.Add("@TID", SqlDbType.Text).Value = TID
                        data = command.ExecuteReader()
                        While data.Read
                            OIDtag = data(0).ToString
                        End While
                    End If

                    data.Close()
                    command.Parameters.Clear()
                    command.CommandText = My.Settings.InsertMovimientoV2
                    command.Parameters.Add("@OIDTag", SqlDbType.Int).Value = OIDtag
                    command.Parameters.Add("@Sentido", SqlDbType.Int).Value = Sentido
                    command.Parameters.Add("@AntennaPort", SqlDbType.Int).Value = AntennaPort
                    If (LastSeenTime.Year < 21) Then
                        LastSeenTime = Date.Now
                    End If
                    command.Parameters.Add("@LastSeenTime", SqlDbType.DateTime).Value = LastSeenTime
                    command.Parameters.Add("@PhaseAngleIRadians", SqlDbType.Float).Value = PhaseAngleInRadians
                    command.Parameters.Add("@PeakRssiInDbm", SqlDbType.Float).Value = PeakRssiInDbm
                    command.Parameters.Add("@SeenCount", SqlDbType.Int).Value = SeenCount

                    data = command.ExecuteReader()
                    While data.Read
                        oidMovimiento = data(0).ToString
                    End While

                End Using

                conn.Close()
            End Using
        Catch ex As Exception

        End Try
    End Sub

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="Sentido"></param>
    ''' <param name="EPC"></param>
    ''' <param name="AntennaPort"></param>
    ''' <param name="LastSeenTime"></param>
    ''' <param name="PhaseAngleInRadians"></param>
    ''' <param name="PeakRssiInDbm"></param>
    ''' <param name="SeenCount"></param>
    ''' <param name="NombreReader">Identificador del reader</param>
    Public Sub InsertMovimientoV2(ByVal Sentido As Integer, ByVal EPC As String, ByVal AntennaPort As Integer, ByVal LastSeenTime As DateTime, ByVal PhaseAngleInRadians As Double, ByVal PeakRssiInDbm As Double, ByVal SeenCount As Double, ByVal TID As String, ByVal NombreReader As String)
        Try
            log.Info("Antes de insertar movimientos")
            Dim OIDtag, oidMovimiento As Integer
            log.Info("Cadena de conexión:" + My.Settings.ConexionSQLV2)
            Using conn As New SqlConnection(My.Settings.ConexionSQLV2)

                conn.Open()
                Using command As New SqlCommand(My.Settings.getTag, conn)
                    Dim data As SqlDataReader


                    command.CommandType = CommandType.StoredProcedure
                    command.Parameters.Add("@EpcHex", SqlDbType.Text).Value = EPC
                    data = command.ExecuteReader()
                    If (data.HasRows) Then
                        While data.Read
                            If data.HasRows = True Then
                                OIDtag = data(0).ToString
                            End If
                        End While
                    Else

                        data.Close()
                        command.Parameters.Clear()
                        command.CommandText = My.Settings.InsertTag
                        command.CommandType = CommandType.StoredProcedure
                        command.Parameters.Add("@EpcHex", SqlDbType.Text).Value = EPC
                        command.Parameters.Add("@TID", SqlDbType.Text).Value = TID
                        data = command.ExecuteReader()
                        While data.Read
                            OIDtag = data(0).ToString
                        End While
                    End If

                    data.Close()
                    command.Parameters.Clear()
                    command.CommandText = My.Settings.InsertMovimientoV2
                    command.Parameters.Add("@OIDTag", SqlDbType.Int).Value = OIDtag
                    command.Parameters.Add("@Sentido", SqlDbType.Int).Value = Sentido
                    command.Parameters.Add("@AntennaPort", SqlDbType.Int).Value = AntennaPort
                    If (LastSeenTime.Year < 21) Then
                        LastSeenTime = Date.Now
                    End If
                    command.Parameters.Add("@LastSeenTime", SqlDbType.DateTime).Value = LastSeenTime
                    command.Parameters.Add("@PhaseAngleIRadians", SqlDbType.Float).Value = PhaseAngleInRadians
                    command.Parameters.Add("@PeakRssiInDbm", SqlDbType.Float).Value = PeakRssiInDbm
                    command.Parameters.Add("@SeenCount", SqlDbType.Int).Value = SeenCount
                    command.Parameters.Add("@Reader", SqlDbType.Text).Value = NombreReader

                    data = command.ExecuteReader()
                    While data.Read
                        oidMovimiento = data(0).ToString
                    End While

                End Using

                conn.Close()
            End Using
        Catch ex As Exception
            log.Info(ex.Message)
            log.Error(ex.Message)
            log.Error(ex.StackTrace)
        End Try
    End Sub

End Class
