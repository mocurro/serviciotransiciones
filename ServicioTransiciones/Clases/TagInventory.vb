﻿Public Class TagInventory
    Private MiEpc As String
    Private MiepcHex As String
    Private Mitid As String
    Private MitidHex As String
    Private Mipc As String
    Private MiantennaPort As String
    Private MiantennaName As String
    Private MipeakRssiCdbm As String
    Private Mifrequency As Double?
    Private MitransmitPowerCdbm As Double?
    Private MilastSeenTime As String
    Private MiphaseAngle As String

    Public Property epc As String
        Get
            Return MiEpc
        End Get
        Set(value As String)
            MiEpc = value
        End Set
    End Property

    Public Property epcHex As String
        Get
            Return MiepcHex
        End Get
        Set(value As String)
            MiepcHex = value
        End Set
    End Property

    Public Property tid As String
        Get
            Return Mitid
        End Get
        Set(value As String)
            Mitid = value
        End Set
    End Property

    Public Property tidHex As String
        Get
            Return MitidHex
        End Get
        Set(value As String)
            MitidHex = value
        End Set
    End Property

    Public Property pc As String
        Get
            Return Mipc
        End Get
        Set(value As String)
            Mipc = value
        End Set
    End Property

    Public Property antennaPort As String
        Get
            Return MiantennaPort
        End Get
        Set(value As String)
            MiantennaPort = value
        End Set
    End Property

    Public Property antennaName As String
        Get
            Return MiantennaName
        End Get
        Set(value As String)
            MiantennaName = value
        End Set
    End Property

    Public Property peakRssiCdbm As String
        Get
            Return MipeakRssiCdbm
        End Get
        Set(value As String)
            MipeakRssiCdbm = value
        End Set
    End Property

    Public Property frequency As Double?
        Get
            Return Mifrequency
        End Get
        Set(value As Double?)
            Mifrequency = value
        End Set
    End Property

    Public Property transmitPowerCdbm As Double?
        Get
            Return MitransmitPowerCdbm
        End Get
        Set(value As Double?)
            MitransmitPowerCdbm = value
        End Set
    End Property

    Public Property lastSeenTime As String
        Get
            Return MilastSeenTime
        End Get
        Set(value As String)
            MilastSeenTime = value
        End Set
    End Property

    Public Property phaseAngle As String
        Get
            Return MiphaseAngle
        End Get
        Set(value As String)
            MiphaseAngle = value
        End Set
    End Property
End Class
