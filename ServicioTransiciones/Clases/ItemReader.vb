﻿Imports MQTTBasic_example

Public Class ItemReader
    Private MiTimestamp As String
    Private MiHostname As String
    Private MitagInventoryEvent As TagInventory
    Private MiErrorResponse As ErrorResponse
    Private MiReaderStatus As ReaderStatus
    Private MiAntennaConnectedEvent As AntennaConnectedEvent
    Private MiAntennaDisconnectedEvent As AntennaDisconnectedEvent


    Public Property timestamp As String
        Get
            Return MiTimestamp
        End Get
        Set(value As String)
            MiTimestamp = value
        End Set
    End Property

    Public Property hostname As String
        Get
            Return MiHostname
        End Get
        Set(value As String)
            MiHostname = value
        End Set
    End Property

    Public Property tagInventoryEvent As TagInventory
        Get
            Return MitagInventoryEvent
        End Get
        Set(value As TagInventory)
            MitagInventoryEvent = value
        End Set
    End Property

    Public Property ErrorResponse As ErrorResponse
        Get
            Return MiErrorResponse
        End Get
        Set(value As ErrorResponse)
            MiErrorResponse = value
        End Set
    End Property

    Public Property ReaderStatus As ReaderStatus
        Get
            Return MiReaderStatus
        End Get
        Set(value As ReaderStatus)
            MiReaderStatus = value
        End Set
    End Property

    Public Property AntennaConnectedEvent As AntennaConnectedEvent
        Get
            Return MiAntennaConnectedEvent
        End Get
        Set(value As AntennaConnectedEvent)
            MiAntennaConnectedEvent = value
        End Set
    End Property

    Public Property AntennaDisconnectedEvent As AntennaDisconnectedEvent
        Get
            Return MiAntennaDisconnectedEvent
        End Get
        Set(value As AntennaDisconnectedEvent)
            MiAntennaDisconnectedEvent = value
        End Set
    End Property
End Class
