﻿Public Class ErrorResponse
    Private Mimessage As String
    Private MiinvalidPropertyId As String

    Public Property message As String
        Get
            Return Mimessage
        End Get
        Set(value As String)
            Mimessage = value
        End Set
    End Property

    Public Property invalidPropertyId As String
        Get
            Return MiinvalidPropertyId
        End Get
        Set(value As String)
            MiinvalidPropertyId = value
        End Set
    End Property
End Class
