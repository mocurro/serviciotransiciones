﻿Imports uPLibrary.Networking.M2Mqtt
Imports uPLibrary.Networking.M2Mqtt.Messages
Imports System.Text
'Imports System.Threading
Imports Transiciones.comun
Imports System.Timers

Public Class Service1

    Dim client As MqttClient
    Dim Msg As StringBuilder = New StringBuilder(4096)
    Public bd As New ConexionBD
    Private listadoItem As New List(Of ItemReader)

    Public WithEvents garbageCollector As New System.Timers.Timer()

    Private miElemento As Dispositivo

    Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

    Protected Overrides Sub OnStart(ByVal args() As String)
        ' Add code here to start your service. This method should set things
        ' in motion so your service can do its work.
        garbageCollector = New System.Timers.Timer

        garbageCollector.Interval = My.Settings.Timegarbage
        garbageCollector.Start()

        IniciarServidor()
    End Sub


    Private Sub IniciarServidor()
        Try
            client = New MqttClient(My.Settings.Servidor)

            Dim clientId As String = "prueba" 'Guid.NewGuid().ToString()
            'Versión 1
            'AddHandler client.MqttMsgPublishReceived, AddressOf Client_MqttMsgPublishReceived
            'Versión 2
            AddHandler client.MqttMsgPublishReceived, AddressOf Client_MqttMsgPublishReceivedReader

            AddHandler client.ConnectionClosed, AddressOf Client_Disconnect

            log.Info("Inicio de servicio con servidor: " + My.Settings.Servidor)
            client.Connect(clientId)

            If (client IsNot Nothing AndAlso client.IsConnected()) Then
                Dim Topic() As String = {"prueba"}
                Dim Qos() As Byte = {0}
                client.Subscribe(Topic, Qos)
            End If

        Catch ex As Exception
            log.Error(ex.Message)
            log.Error(ex.StackTrace)
            System.Threading.Thread.Sleep(5000)
            IniciarServidor()
        End Try
    End Sub

    Private Sub Client_MqttMsgPublishReceived(ByVal sender As Object, ByVal e As MqttMsgPublishEventArgs)
        Dim msj = Encoding.Default.GetString(e.Message)
        Msg.AppendLine()
        Msg.Append("[" + TimeOfDay.ToString("h:mm:ss tt") + "] Topic: " + e.Topic.ToString() + ", Len: " + e.Message.Length.ToString() + ", Qos: " + e.QosLevel.ToString())
        Msg.AppendLine()
        Msg.Append("Msg : " + msj)
        Msg.AppendLine()
        log.Info(msj)

        Try
            Dim Testobject As ItemReader = Newtonsoft.Json.JsonConvert.DeserializeObject(Of ItemReader)(msj)
            If (Testobject IsNot Nothing) Then

                If miElemento Is Nothing Then
                    If Not String.IsNullOrEmpty(Testobject.hostname) Then
                        miElemento = bd.getDispositivo(Testobject.hostname)
                    End If
                End If

                If Testobject.tagInventoryEvent IsNot Nothing Then
                    Dim listaElementos = listadoItem.Where(Function(x) x.tagInventoryEvent.epcHex = Testobject.tagInventoryEvent.epcHex)


                    If listaElementos Is Nothing OrElse listaElementos.Count = 0 Then
                        listadoItem.Add(Testobject)
                        bd.getElementoOrden(Testobject.tagInventoryEvent, miElemento)
                    End If
                End If
                If Testobject.InventoryStatusEvent IsNot Nothing Then
                    If miElemento IsNot Nothing Then
                        bd.InsertReaderEstatus(miElemento, Testobject.timestamp, IIf(Testobject.InventoryStatusEvent.inventoryStatus.Equals("running"), 1, 0), Testobject.InventoryStatusEvent.inventoryStatus)
                    End If
                End If

                If Testobject.AntennaConnectedEvent IsNot Nothing Then
                    If miElemento IsNot Nothing Then
                        bd.InsertReaderEstatus(miElemento, Testobject.timestamp, 1, "Antena conectada, puerto: " + Testobject.AntennaConnectedEvent.antennaPort)
                    End If
                End If

                If Testobject.AntennaDisconnectedEvent IsNot Nothing Then
                    If miElemento IsNot Nothing Then
                        bd.InsertReaderEstatus(miElemento, Testobject.timestamp, 0, "Antena conectada, puerto: " + Testobject.AntennaConnectedEvent.antennaPort)
                    End If

                End If
            End If

        Catch ex As Exception
            log.Error(ex.Message)
            log.Error(ex.StackTrace)

        End Try

    End Sub

    Private Sub Client_MqttMsgPublishReceivedReader(ByVal sender As Object, ByVal e As MqttMsgPublishEventArgs)
        Dim msj = Encoding.Default.GetString(e.Message)
        Dim listado = msj.Split(",")
        Dim elementos As String()
        Dim EPC = "", TID = "", NombreReader = ""
        Dim AntennaPort, Sentido As Integer
        Dim LastSeenTime As DateTime
        Dim PhaseAngleInRadians, PeakRssiInDbm, SeenCount As Double
        log.Info(msj)
        Try

            For Each elemento In listado
                log.Info("cantidad:" + listado.Count.ToString)
                Dim dato = elemento.Split(":")
                Select Case dato(0)
                    Case "Sentido"
                        If (dato(1).Equals(" Entrada") Or dato(1).Equals("Entrada")) Then
                            Sentido = 1
                        ElseIf (dato(1).Equals(" Salida") Or dato(1).Equals("Salida")) Then
                            Sentido = -1
                        Else
                            Sentido = 0
                        End If
                    Case " EPC"
                        EPC = dato(1).Replace(" ", "")
                    Case " Antenna Port"
                        AntennaPort = dato(1)
                    Case " LastSeenTime"
                        LastSeenTime = New DateTime(dato(1))
                    Case " PhaseAngleInRadians"
                        PhaseAngleInRadians = dato(1)
                    Case " PeakRssiInDbm"
                        PeakRssiInDbm = dato(1)
                    Case " TID"
                        TID = dato(1)
                    Case " SeenCount "
                        SeenCount = dato(1)
                    Case " NombreReader "
                        NombreReader = dato(1)

                    Case Else

                End Select

            Next

            'log.Info("Guardando..." + "" + Sentido + EPC.Trim + AntennaPort + LastSeenTime.ToString() + PhaseAngleInRadians + PeakRssiInDbm + SeenCount + TID.Trim + NombreReader)
            'prueba Sentido: , EPC: 5645 33B2 DDD9 0140 0000 0000, Antenna Port: 1, LastSeenTime: 1621564834201393, PhaseAngleInRadians: 5.197126909356553, PeakRssiInDbm: -77, TID: E280 1130 2000 3958 B2F7 08DA
            bd.InsertMovimientoV2(Sentido, EPC.Trim, AntennaPort, LastSeenTime, PhaseAngleInRadians, PeakRssiInDbm, SeenCount, TID.Trim, NombreReader)
        Catch ex As Exception
            log.Info(ex.Message)
            log.Error(ex.Message)
            log.Error(ex.StackTrace)
        End Try
    End Sub

    Protected Overrides Sub OnStop()
        ' Add code here to perform any tear-down necessary to stop your service.
        Try
            If (client IsNot Nothing) Then
                client.Disconnect()
                client = Nothing
            End If
            garbageCollector.Stop()
        Catch ex As Exception
            log.Error(ex.Message)
            log.Error(ex.StackTrace)
        End Try

    End Sub

    Private Sub Client_Disconnect(sender As Object, e As EventArgs)
        IniciarServidor()
    End Sub


    Private Sub garbageCollector_Elapsed(sender As Object, e As ElapsedEventArgs) Handles garbageCollector.Elapsed
        Try
            For Each elemento In listadoItem
                If (elemento.Borrar) Then
                    listadoItem.Remove(elemento)
                Else
                    elemento.Borrar = True
                End If
            Next

        Catch ex As Exception
            log.Error(ex.Message)
            log.Error(ex.StackTrace)
        End Try
    End Sub
End Class
